<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "accounts".
 *
 * @property int $id
 * @property int|null $client_uid
 * @property int|null $login
 */
class Accounts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'accounts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_uid', 'login'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'client_uid' => Yii::t('app', 'Client Uid'),
            'login' => Yii::t('app', 'Login'),
        ];
    }

    public function getUsers()
    {
        return $this->hasOne(Users::className(), ['client_uid' => 'client_uid']);
    }

    public static function receivesLogin($clientUid): string
    {
        $model =
            self::find()
                ->select(['client_uid','login'])
                ->distinct()
                ->where(['client_uid' => $clientUid])
                ->asArray()
                ->all()
        ;

        if (empty($model)) {
            return '';
        }

        return implode(',', array_column($model, 'login'));
    }
}
