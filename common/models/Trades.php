<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "trades".
 *
 * @property int $id
 * @property int|null $ticket
 * @property int|null $login
 * @property string|null $symbol
 * @property int|null $cmd
 * @property float|null $volume
 * @property string|null $open_time
 * @property string|null $close_time
 * @property float|null $profit
 * @property float|null $coeff_h
 * @property float|null $coeff_cr
 */
class Trades extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trades';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ticket', 'login', 'cmd'], 'integer'],
            [['volume', 'profit', 'coeff_h', 'coeff_cr'], 'number'],
            [['open_time', 'close_time'], 'safe'],
            [['symbol'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ticket' => Yii::t('app', 'Ticket'),
            'login' => Yii::t('app', 'Login'),
            'symbol' => Yii::t('app', 'Symbol'),
            'cmd' => Yii::t('app', 'Cmd'),
            'volume' => Yii::t('app', 'Volume'),
            'open_time' => Yii::t('app', 'Open Time'),
            'close_time' => Yii::t('app', 'Close Time'),
            'profit' => Yii::t('app', 'Profit'),
            'coeff_h' => Yii::t('app', 'Coeff H'),
            'coeff_cr' => Yii::t('app', 'Coeff Cr'),
        ];
    }

    /**
     * @param string $login
     * @param string $open_time
     * @param string $close_time
     * @return mixed[]
     */
    public static function receivesTrades(string $login, string $open_time, string $close_time): array
    {

        return
            self::find()
                ->select(['total_volume' => '(coeff_h * coeff_cr * volume)', 'profit','open_time','close_time'])
                ->where(['login' => $login])
                ->andFilterWhere(['>=', 'open_time', $open_time])
                ->andFilterWhere(['<=', 'close_time', $close_time])
                ->asArray()
                ->all()
            ;
    }
}
