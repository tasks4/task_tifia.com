<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property int|null $client_uid
 * @property string|null $email
 * @property string|null $gender
 * @property string|null $fullname
 * @property string|null $country
 * @property string|null $region
 * @property string|null $city
 * @property string|null $address
 * @property int|null $partner_id
 * @property string|null $reg_date
 * @property int|null $status
 */
class Users extends \yii\db\ActiveRecord
{
    const  STATUS_ACTIVATE = 1;
    const  STATUS_INACTIVATE = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_uid', 'partner_id', 'status'], 'integer'],
            [['reg_date'], 'safe'],
            [['email'], 'string', 'max' => 100],
            [['gender'], 'string', 'max' => 5],
            [['fullname'], 'string', 'max' => 150],
            [['country'], 'string', 'max' => 2],
            [['region', 'city'], 'string', 'max' => 50],
            [['address'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'client_uid' => Yii::t('app', 'Client Uid'),
            'email' => Yii::t('app', 'Email'),
            'gender' => Yii::t('app', 'Gender'),
            'fullname' => Yii::t('app', 'Fullname'),
            'country' => Yii::t('app', 'Country'),
            'region' => Yii::t('app', 'Region'),
            'city' => Yii::t('app', 'City'),
            'address' => Yii::t('app', 'Address'),
            'partner_id' => Yii::t('app', 'Partner ID'),
            'reg_date' => Yii::t('app', 'Reg Date'),
            'status' => Yii::t('app', 'Status'),
        ];
    }


    public function getAccounts()
    {
        return $this->hasMany(Accounts::className(), ['client_uid' => 'client_uid']);
    }

    /**
     * @return mixed[]
     */
    public static function receivesUsers(): array
    {
        return
            self::find()
                ->select(['users.fullname', 'users.partner_id', 'users.client_uid'])
                ->distinct()
                ->andWhere(['not', ['users.partner_id' => null]])
                ->where(['users.status' => self::STATUS_ACTIVATE])
                ->asArray()
                ->all()
            ;
    }
}
