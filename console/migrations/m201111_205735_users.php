<?php

use yii\db\Migration;

/**
 * Class m201111_205735_users
 */
class m201111_205735_users extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201111_205735_users cannot be reverted.\n";

        return false;
    }
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%users}}',
            [
                'id' => $this->primaryKey(),
                'client_uid' => $this->string(),
                'email' => $this->string(),
                'gender' => $this->string(32),
                'fullname' => $this->string(),
                'country' => $this->string(),
                'region' => $this->string(),
                'city' => $this->string(),
                'address' => $this->string(),
                'partner_id' => $this->integer()->defaultValue(10),
                'reg_date' => $this->dateTime(),
                'status' => $this->tinyInteger()->notNull(),
            ],
            $tableOptions
        );
    }

    public function down()
    {
        $this->dropTable('{{%users}}');
    }

}
