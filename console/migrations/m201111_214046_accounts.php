<?php

use yii\db\Migration;

/**
 * Class m201111_214046_accounts
 */
class m201111_214046_accounts extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201111_214046_accounts cannot be reverted.\n";

        return false;
    }

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%accounts}}',
            [
                'id' => $this->primaryKey(),

            ],
            $tableOptions
        );
    }

    public function down()
    {
        $this->dropTable('{{%accounts}}');
    }
}
