<?php

use yii\db\Migration;

/**
 * Class m201111_214335_treades
 */
class m201111_214335_trades extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201111_214335_treades cannot be reverted.\n";

        return false;
    }

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%trades}}',
            [
                'id' => $this->primaryKey(),
            ],
            $tableOptions
        );
    }

    public function down()
    {
        $this->dropTable('{{%trades}}');
    }
}
