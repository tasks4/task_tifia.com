<?php

namespace frontend\components;

use common\models\Users;
use yii\base\Component;

class UsersComponent extends Component
{
    /**
     * @var string
     */
    private $usersTree = '';

    /**
     * @var array|mixed[]
     */
    public $listUsers = [];

    /**
     * @var array
     */
    private $listSortedUsers = [];

    /**
     * UserTree constructor.
     * @param mixed $config
     */
    public function __construct($config = [])
    {
        $this->listUsers = Users::receivesUsers();
        $this->listSortedUsers = $this->sortUsers();

        parent::__construct($config);
    }

    /**
     * @return mixed[]
     */
    public function sortUsers(): array
    {
        $list = [];

        foreach ($this->listUsers as $user) {
            if (empty($list[$user['partner_id']])) {
                $list[$user['partner_id']] = [];
            }

            $list[$user['partner_id']][] = $user;
        }

        return $list;
    }

    /**
     * @param int $partnerId
     * @return string
     */
    public function customBuildTree(int $partnerId = 0): string
    {
        $users = $this->sortUsers();

        if (empty($users[$partnerId])) {
            $this->usersTree .= '</li>';

            return '';
        }

        $this->usersTree .= '<ul>';

        foreach ($users[$partnerId] as $user) {
            $this->usersTree =
                sprintf(
                    '%s  <li> %s | Client UID - %s',
                    $this->usersTree,
                    $user['fullname'],
                    $user['client_uid']
                );
            $this->customBuildTree($user['client_uid']);

            $this->usersTree .= '</li>';
        }

        return $this->usersTree .= '</ul>';
    }

    /**
     * @param int $partnerId
     * @return mixed[]
     */
    public function receiveItems($partnerId = 0): array
    {
        $menuItems = [];
        $listSortedUsers = $this->listSortedUsers;

        if (empty($listSortedUsers[$partnerId])) {
            return $menuItems;
        }

        foreach ($listSortedUsers[$partnerId] as $user) {
            $menuItems[$user['client_uid']] =
                [
                    'label' =>
                        sprintf(
                            "%s  %s",
                            $user['fullname'],
                            $this->countReferralUsers($listSortedUsers[$user['client_uid']] ?? [])
                        ),
                    'items' => $this->receiveItems($user['client_uid']),
                ];
        }

        return $menuItems;
    }

    /**
     * @param array $users
     * @return string
     */
    private function countReferralUsers(array $users): string
    {
        return !empty($users) ? sprintf('(%s)', count($users)) : '';
    }
}
