<?php

namespace frontend\controllers;

use common\models\Accounts;
use common\models\Trades;
use frontend\components\UsersComponent;
use frontend\models\CalculateTrades;
use yii\web\Controller;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index', ['usersComponent' => new  UsersComponent()]);
    }

    /**
     * @return mixed
     */
    public function actionCalculateTrades()
    {
        $model = new CalculateTrades();
        $accounts =
            Accounts::find()
                ->asArray()
                ->limit(5000)
                ->all()
        ;

        $result = [];

        if ($model->load(\Yii::$app->request->post())) {
            $result = Trades::receivesTrades($model->login, $model->start_date, $model->end_date);
        }

        return $this->render('calculate-trades', compact('model', 'result', 'accounts'));
    }
}
