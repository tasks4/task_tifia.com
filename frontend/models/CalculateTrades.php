<?php

namespace frontend\models;

use yii\base\Model;

class CalculateTrades extends Model
{
    public $login;

    public $start_date;

    public $end_date;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return
            [
                [['login', 'start_date', 'end_date'], 'required'],
                [[ 'start_date', 'end_date'], 'safe'],
            ];
    }
}
