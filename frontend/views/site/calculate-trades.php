<?php
/**
 * @var $model  \frontend\models\CalculateTrades
 * @var $result
 * @var $accounts \common\models\Accounts
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use \kartik\date\DatePicker;

set_time_limit(1000);

$usersComponent = Yii::$app->usersComponent;

$list = [];

foreach ($accounts as $user) {
    $userId = array_search($user['client_uid'], array_column($usersComponent->listUsers, 'client_uid'));

    if ($userId !== false) {
        $list[$user['login']] =
            sprintf(
                "Login:%s Full Name: %s Client UID:%s ",
                $user['login'],
                $usersComponent->listUsers[$userId]['fullname'],
                $user['client_uid']
            );
    }
}

?>
<pre>
<div class="row">
    <div class="col-lg-5">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'login')->widget(
            \kartik\select2\Select2::classname(), [
            'data' => $list,
            'options' => ['placeholder' => 'Users'],
            'pluginOptions' => ['allowClear' => true],
        ]) ?>

        <?= $form->field($model, 'start_date')->widget(
            DatePicker::classname(), [
                'type' => DatePicker::TYPE_RANGE,
                'attribute2' => 'end_date',
                'separator' => '<i class="glyphicon glyphicon-resize-horizontal"></i>',
                'layout' => '',
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-d',

                ],

            ]
        )
        ; ?>

        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'calculate-trades-button']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>


<?php
if (!empty($result)) { ?>
    <h1> <?= preg_replace('~.*Name\:\s*|Client.*~sui', '', $list[$model->login] ?? '') ?></h1>
    <h2>Прибыльность: <?= ceil(array_sum(array_column($result, 'profit'))) ?></h2>
    <?php
    foreach ($result as $trad) {
        ?>
        <p>Cуммарный объем: <?= $trad['total_volume'] ?> | <?= $trad['open_time'] ?> - <?= $trad['close_time'] ?>   </p>
        <?php
    }
}
?>
</pre>
