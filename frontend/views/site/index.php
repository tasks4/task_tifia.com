<?php

/**
 * @var usersComponent  \frontend\components\UsersComponent
 */

use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

NavBar::begin();
echo Nav::widget(['items' => Yii::$app->usersComponent->receiveItems()]);
NavBar::end();
